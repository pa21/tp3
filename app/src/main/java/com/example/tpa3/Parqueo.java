package com.example.tpa3;

public class Parqueo {

    private int id;
    private Usuario usuario;
    private String nro_matricula;
    private int tiempo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_usuario() { return usuario.getId();}

    public void setId_usuario(Usuario usuario) {
        this.usuario=usuario;}

    public String getNro_matricula() {
        return nro_matricula;
    }

    public void setNro_matricula(String nro_matricula) {
        this.nro_matricula = nro_matricula;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }
}
