package com.example.tpa3;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class CuadroDialogo {

    public interface FinalCuadroDialogo{
        void ResultadoCuadroDialogo(String matricula,String tiempo);
    }

    private FinalCuadroDialogo interfaz;

    public CuadroDialogo(final Context contexto,FinalCuadroDialogo actividad){
        interfaz = actividad;
        final Dialog dialogo = new Dialog(contexto);
        dialogo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogo.setCancelable(false);
        dialogo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialogo.setContentView(R.layout.custom_dialog);

        final EditText TextMatricula = (EditText) dialogo.findViewById(R.id.nro_matricula);
        final EditText TextTiempo = (EditText) dialogo.findViewById(R.id.tiempo);
        Button btnRegistrar = (Button) dialogo.findViewById(R.id.registrar);
        Button btnCancelar = (Button) dialogo.findViewById(R.id.cancelar);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interfaz.ResultadoCuadroDialogo(TextMatricula.getText().toString(),TextTiempo.getText().toString());
                dialogo.dismiss();

            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogo.dismiss();
            }
        });

        dialogo.show();
    }
}
