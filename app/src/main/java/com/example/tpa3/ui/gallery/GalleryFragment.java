package com.example.tpa3.ui.gallery;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.tpa3.DBInfo;
import com.example.tpa3.DataAccess;
import com.example.tpa3.NavigationDrawer;
import com.example.tpa3.R;
import com.example.tpa3.Usuario;
import com.example.tpa3.utils.Session;

public class GalleryFragment extends Fragment {

    //private GalleryViewModel galleryViewModel;
    Session sesion;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);

        View root = inflater.inflate(R.layout.fragment_gallery, container, false);

        sesion = new Session(this.getActivity());
        if(sesion.is_active()){

        TextView txtNombre = root.findViewById(R.id.txtNombreUs);
        TextView txtEmail = root.findViewById(R.id.txtEmailUs);
        TextView txtPass = root.findViewById(R.id.txtIdUs);
        txtNombre.setText(sesion.getSession().getNombre());
        txtEmail.setText(sesion.getSession().getEmail());
        txtPass.setText(String.valueOf(sesion.getSession().getId()));

        }
        /*galleryViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        */

        return root;

    }
}