package com.example.tpa3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import org.w3c.dom.Text;

import java.util.ArrayList;

import com.example.tpa3.Registro;
import com.example.tpa3.ui.home.HomeFragment;
import com.example.tpa3.utils.Session;

public class MainActivity extends AppCompatActivity {

    Session sesion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sesion = new Session(getApplicationContext());
    }

    public void onClickRegistrarse(View view){
        exec_activity(null, Registro.class);
    }

    public void onClickIniciarSesion(View view){
        TextView nombreUsuario = (TextView)findViewById(R.id.nombreUsuario);
        TextView password = (TextView)findViewById(R.id.password);
        String User = nombreUsuario.getText().toString();
        String Pass = password.getText().toString();

        if(User.isEmpty() || Pass.isEmpty()){
            String errmsj = getResources().getString(R.string.campos_incompletos);
            Toast.makeText(getApplicationContext(), errmsj, errmsj.length()).show();
            return;
        }

        DataAccess da = new DataAccess(this, DBInfo.UsuarioNombreDB, null, 1);
        Usuario UserDB = da.getUsuario(User);

        if(UserDB != null){
            if( ! Pass.equals( UserDB.getPasswd().toString() ) ){ 
                String errmsj = getResources().getString(R.string.passwd_no_coinciden);
                Toast.makeText(getApplicationContext(), errmsj, errmsj.length()).show();
                return;
            }

            sesion.logIn(UserDB);

            Toast.makeText(this,"Logueado con Exito",Toast.LENGTH_LONG).show();

            SharedPreferences preferences = getSharedPreferences("usuario", Context.MODE_PRIVATE);
            SharedPreferences.Editor objEdit = preferences.edit();
            objEdit.putString("id",String.valueOf(UserDB.getId()));
            objEdit.putString("nombre",UserDB.getNombre().toString());
            objEdit.putString("email",UserDB.getEmail().toString());
            objEdit.commit();

            exec_activity(null,NavigationDrawer.class);
        }else{
            Toast.makeText(this,"Usuario no Registrado",Toast.LENGTH_LONG).show();
        }


    }


    private void exec_activity(View view, Class dest){
        //Toast.makeText(getApplicationContext(),et.getText() , et.getText().length()).show();
        Intent i = new Intent(this, dest);
        startActivity(i);
        //MainActivity.this.finish();
    }

    @Override
    public void onBackPressed() {

        //super.onBackPressed();
    }
}