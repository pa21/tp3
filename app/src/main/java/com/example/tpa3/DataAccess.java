package com.example.tpa3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.tpa3.utils.Session;

import java.util.ArrayList;

;

public class DataAccess extends SQLiteOpenHelper {


    public DataAccess(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Crea las tablas
        String query= DBInfo.UsuarioCreaciontable;
        db.execSQL(query);
        query= DBInfo.ParqueosCreaciontable;
        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //Metodo para abrir
    public void abrir()
    {
        this.getWritableDatabase();
    }

    public void cerrar()
    {
        this.close();
    }

    //Método que me permite insertar registros en la tabla usuarios

    public void insertarUsuario(Usuario u)
    {
        ContentValues data = new ContentValues();
        data.put(DBInfo.UsuariosColumnaNombre,u.getNombre());
        data.put(DBInfo.UsuariosColumnaEmail,u.getEmail());
        data.put(DBInfo.UsuariosColumnaPasswd,u.getPasswd());
        this.getWritableDatabase().insert(DBInfo.UsuarioTable,null, data);
    }

    public void insertarParqueo(Parqueo p)
    {
        ContentValues data = new ContentValues();
        data.put(DBInfo.ParqueosColumnaUsuario, p.getId_usuario());
        data.put(DBInfo.ParqueosColumnaNroMatricula, p.getNro_matricula());
        data.put(DBInfo.ParqueosColumnaTiempo, p.getTiempo());
        this.getWritableDatabase().insert(DBInfo.ParqueosTable,null, data);
    }


    public void eliminarUsuario(Usuario u)
    {
        ContentValues data = new ContentValues();
        data.put(DBInfo.UsuariosColumnaID, u.getId());
        String id = String.valueOf(u.getId());
        this.getWritableDatabase().delete(DBInfo.UsuarioTable,"_ID=?",new String[]{id});
    }

    public void eliminarParqueo(Parqueo p)
    {
        ContentValues data = new ContentValues();
        data.put(DBInfo.ParqueosColumnaID, p.getId());
        String id = String.valueOf(p.getId());
        this.getWritableDatabase().delete(DBInfo.ParqueosTable,"_ID=?",new String[]{id});
    }

    public ArrayList<Usuario> getListadoUsuario()
    {
        ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();
        Cursor mcursor = null;
        mcursor = this.getReadableDatabase().query(DBInfo.UsuarioTable, new String[]{DBInfo.UsuariosColumnaID,DBInfo.UsuariosColumnaNombre,DBInfo.UsuariosColumnaEmail,DBInfo.UsuariosColumnaPasswd},null,null,null,null,null);
        if(mcursor.moveToFirst())
        {
            do{
                Usuario u = new Usuario();
                u.setId(mcursor.getInt(0));
                u.setNombre(mcursor.getString(1));
                u.setEmail(mcursor.getString(2));
                u.setPasswd(mcursor.getString(3));
                listaUsuarios.add(u);
            }
            while(mcursor.moveToNext());
        }
        return listaUsuarios;
    }

    public ArrayList<Parqueo> getListadoParqueo()
    {
        ArrayList<Parqueo> listaParqueos = new ArrayList<Parqueo>();
        Cursor mcursor = null;
        mcursor = this.getReadableDatabase().query(DBInfo.ParqueosTable, new String[]{DBInfo.ParqueosColumnaID,DBInfo.ParqueosColumnaUsuario,DBInfo.ParqueosColumnaNroMatricula,DBInfo.ParqueosColumnaTiempo},null,null,null,null,null);

        if(mcursor.moveToFirst())
        {
            do{
                Parqueo p = new Parqueo();
                Usuario u = new Usuario();
                p.setId(mcursor.getInt(0));
                u.setId(mcursor.getInt(1));
                p.setId_usuario(u);
                p.setNro_matricula(mcursor.getString(2));
                p.setTiempo(mcursor.getInt(3));
                listaParqueos.add(p);
            }
            while(mcursor.moveToNext());
        }
        return listaParqueos;
    }

    ///Se me hace logico buscar solo el usuario , que todoo el array
    public Usuario getUsuario(String Usuario)
    {
        Usuario User = new Usuario();
        Cursor fila;
        fila = this.getReadableDatabase().rawQuery("select * from usuarios where nombre='" + Usuario + "'", null);
        if(fila.moveToFirst())
        {
            User.setId(fila.getInt(0));
            User.setNombre(fila.getString(1));
            User.setEmail(fila.getString(2));
            User.setPasswd(fila.getString(3));
        }else{
            return null;
        }

        return User;
    }

}

