package com.example.tpa3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tpa3.ui.home.HomeFragment;
import com.example.tpa3.utils.Session;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class NavigationDrawer extends AppCompatActivity implements CuadroDialogo.FinalCuadroDialogo{

    private AppBarConfiguration mAppBarConfiguration;
    Session sesion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);

        /*SharedPreferences ss = getSharedPreferences("usuario",MODE_PRIVATE);
        String nombre = ss.getString("nombre","").toString();
        String email = ss.getString("email","").toString();
        Integer uid = Integer.parseInt(ss.getString("id","").toString()); */
        sesion = new Session(this.getApplicationContext());
        String nombre = sesion.getSession().getNombre();
        String email = sesion.getSession().getEmail();
        int uid = sesion.getSession().getId();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                 //       .setAction("Action", null).show();
                //CustomDialogClass cdd = new CustomDialogClass(NavigationDrawer.this);
                //cdd.show();
                new CuadroDialogo(NavigationDrawer.this,NavigationDrawer.this);
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        //CON ESTO GENERAMOS LOS CONTROLES EN EL HEADER DEL MENU
        View hView = navigationView.getHeaderView(0);
        TextView txtUser = (TextView) hView.findViewById(R.id.nombreUserNav); //Traemos los controles
        TextView txtEmail = (TextView) hView.findViewById(R.id.emailUserNav) ;
        //Y aca los cargamos con la seion guardada al iniciar sesion

        txtUser.setText(nombre);
        txtEmail.setText(email);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void ResultadoCuadroDialogo(String matricula, String tiempo) {
        SharedPreferences ss = getSharedPreferences("usuario",MODE_PRIVATE);
        String id = ss.getString("id","").toString();
        if(matricula.isEmpty() || tiempo.isEmpty()){
            String errmsj = getResources().getString(R.string.campos_incompletos);
            Toast.makeText(getApplicationContext(), errmsj, errmsj.length()).show();
            return;
        }
        sesion = new Session(this.getApplicationContext());
        Usuario usuario = sesion.getSession();
        DataAccess da = new DataAccess(this, DBInfo.UsuarioNombreDB, null, 1);
        Parqueo npar = new Parqueo();

        npar.setId_usuario(usuario);
        npar.setNro_matricula(matricula);
        npar.setTiempo(Integer.parseInt(tiempo));
        da.insertarParqueo(npar);
        

        //TextView txt = (TextView) findViewById(R.id.text_home);
        //txt.setText(matricula);
        //String msg = "Registrado correctamente : " + matricula + " - " + tiempo + " - " + id;
        String msg = "Parqueo registrado correctamente";
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
        startActivity(new Intent(this.getApplicationContext(), NavigationDrawer.class));
    }
}