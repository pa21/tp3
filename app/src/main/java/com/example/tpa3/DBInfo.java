package com.example.tpa3;


public class DBInfo {

    public static String UsuarioNombreDB="db_usuarios";
    public static String UsuarioCreaciontable="CREATE TABLE IF NOT EXISTS usuarios(_ID integer primary key autoincrement, nombre text, email text, passwd text);";
    public static String UsuarioTable="usuarios";
    public static String UsuariosColumnaID="_ID";
    public static String UsuariosColumnaNombre="nombre";
    public static String UsuariosColumnaEmail="email";
    public static String UsuariosColumnaPasswd="passwd";

    public static String ParqueosCreaciontable="CREATE TABLE IF NOT EXISTS parqueos(_ID integer primary key autoincrement, id_usuario integer, nro_matricula text, tiempo int);";
    public static String ParqueosTable="parqueos";
    public static String ParqueosColumnaID="_ID";
    public static String ParqueosColumnaUsuario="id_usuario";
    public static String ParqueosColumnaNroMatricula="nro_matricula";
    public static String ParqueosColumnaTiempo="tiempo";


}