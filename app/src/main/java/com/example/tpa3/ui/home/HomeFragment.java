package com.example.tpa3.ui.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tpa3.DBInfo;
import com.example.tpa3.DataAccess;
import com.example.tpa3.MainActivity;
import com.example.tpa3.Parqueo;
import com.example.tpa3.R;
import com.example.tpa3.Usuario;
import com.example.tpa3.adapters.AdapterParqueos;
import com.example.tpa3.utils.Session;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    Session sesion;
   // private HomeViewModel homeViewModel;
    RecyclerView rvItemsParqueos;
    RecyclerView.LayoutManager layoutManager;
    AdapterParqueos adapterParqueos;
    ArrayList<Parqueo> listaParqueos, listaFiltrada;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        //homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);


        View root = inflater.inflate(R.layout.fragment_home, container, false);

        sesion = new Session(this.getActivity());
        if(sesion.is_active()){

            Usuario usuario = sesion.getSession();
            int idUsuario = usuario.getId();

            rvItemsParqueos = root.findViewById(R.id.rvItemsParqueos);
            layoutManager = new GridLayoutManager(getContext(), 2);
            rvItemsParqueos.setLayoutManager(layoutManager);

            DataAccess da = new DataAccess(getContext(), DBInfo.UsuarioNombreDB, null, 1);
            listaParqueos = da.getListadoParqueo();

            listaFiltrada = new ArrayList<>();

            for(Parqueo registro : listaParqueos){
                //if(registro.getId() == idUsuario){
                if(registro.getId_usuario() == idUsuario){
                    listaFiltrada.add(registro);
                }
            }


            adapterParqueos = new AdapterParqueos(listaFiltrada);
            rvItemsParqueos.setAdapter(adapterParqueos);
            setHasOptionsMenu(true);
        } else {
            logOut();
        }


        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                //nada.
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar_sesion:
                Toast.makeText(getContext(),"Se cerró la sesión del usuario",Toast.LENGTH_LONG).show();
                logOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void logOut()
    {
        //Logout
        sesion.logOut();
        startActivity(new Intent(getContext(), MainActivity.class));
    }


}