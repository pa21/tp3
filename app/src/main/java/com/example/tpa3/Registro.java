package com.example.tpa3;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;

public class Registro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);
    }
    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
    public void onClickAceptar(View view){
        DataAccess da = new DataAccess(this, DBInfo.UsuarioNombreDB, null, 1);
        ArrayList<Usuario> listadoUsuarios = new ArrayList<Usuario>();
        listadoUsuarios = da.getListadoUsuario();
        TextView nombre = (TextView)findViewById(R.id.nombre);
        TextView email = (TextView)findViewById(R.id.email);
        TextView passwd = (TextView)findViewById(R.id.passwd);
        TextView passwd2 = (TextView)findViewById(R.id.passwd2);

        String nombreText = nombre.getText().toString();
        String emailText = email.getText().toString();
        String passwdText = passwd.getText().toString();
        String passwd2Text = passwd2.getText().toString();

        String errmsj="";

        if( nombreText.isEmpty() || emailText.isEmpty() || passwdText.isEmpty() || passwd2Text.isEmpty() ){
            errmsj = getResources().getString(R.string.campos_incompletos);
            Toast.makeText(getApplicationContext(), errmsj, errmsj.length()).show();
            return;
        }


        if( ! passwdText.equals( passwd2Text ) ){
            errmsj = getResources().getString(R.string.passwd_no_coinciden);
            Toast.makeText(getApplicationContext(), errmsj, errmsj.length()).show();
            return;
        }


        Iterator it = listadoUsuarios.iterator();
        Usuario usu = new Usuario();
        boolean mail = true, usuario = true;
        while(it.hasNext() && mail && usuario){
            usu = (Usuario) it.next();

            if(usu.getEmail().equals(emailText)){
                mail = false;
            }
            if(usu.getNombre().equals(nombreText)){
                usuario = false;
            }
        }
        if(!mail){
            errmsj = getResources().getString(R.string.email_repetido);
            Toast.makeText(getApplicationContext(), errmsj, errmsj.length()).show();
            return;
        }
        if(!usuario){
            errmsj = getResources().getString(R.string.usuario_repetido);
            Toast.makeText(getApplicationContext(), errmsj, errmsj.length()).show();
            return;
        }
        if (!validarEmail(emailText)){
            errmsj = "Email invalido";
            Toast.makeText(getApplicationContext(), errmsj, errmsj.length()).show();
            return;
        }
        if (errmsj=="")
       { usu.setNombre(nombreText);
        usu.setEmail(emailText);
        usu.setPasswd(passwdText);
        da.insertarUsuario(usu);
        String msj = getResources().getString(R.string.registro_usuario_ok);
        Toast.makeText(getApplicationContext(), msj, msj.length()).show();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);}
    }
}

