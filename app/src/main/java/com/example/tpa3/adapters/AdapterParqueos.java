package com.example.tpa3.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tpa3.Parqueo;
import com.example.tpa3.R;


import java.util.ArrayList;

public class AdapterParqueos extends RecyclerView.Adapter<AdapterParqueos.ViewHolderDatos>{

    ArrayList<Parqueo>  listDatos;

    public AdapterParqueos(ArrayList<Parqueo> listDatos) {
        this.listDatos = listDatos;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parqueo_item_view, parent, false);

        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos holder, int position) {
        holder.tvNroMatricula.setText(listDatos.get(position).getNro_matricula());
        holder.tvTiempo.setText(listDatos.get(position).getTiempo()+"");
    }

    @Override
    public int getItemCount() {
        return listDatos.size();
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {

        TextView tvNroMatricula;
        TextView tvTiempo;

        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            tvNroMatricula = itemView.findViewById(R.id.tvNroMatricula);
            tvTiempo = itemView.findViewById(R.id.tvTiempo);
        }


    }
}
