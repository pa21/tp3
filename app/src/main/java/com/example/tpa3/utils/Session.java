package com.example.tpa3.utils;

//Basado en:
//https://stackoverflow.com/questions/20678669/how-to-maintain-session-in-android


import android.content.Context;
import android.content.SharedPreferences;


import com.example.tpa3.DBInfo;
import com.example.tpa3.Usuario;

public class Session {
    private SharedPreferences sesion;
    private SharedPreferences.Editor editor;
    private Context context;
    private static final String _IS_ACTIVE = "IS_ACTIVE";
    private boolean IS_ACTIVE;


    public Session(Context _context) {
        sesion = _context.getSharedPreferences(DBInfo.UsuarioTable, Context.MODE_PRIVATE);
        editor = sesion.edit();
        this.context = _context;
    }

    public void logIn(Usuario usuario) {

        editor.putInt(DBInfo.UsuariosColumnaID,usuario.getId());
        editor.putString(DBInfo.UsuariosColumnaNombre,(usuario.getNombre()));
        editor.putString(DBInfo.UsuariosColumnaEmail,(usuario.getEmail()));
        editor.putString(DBInfo.UsuariosColumnaEmail,(usuario.getEmail()));
        editor.putBoolean(_IS_ACTIVE, true);
        editor.commit();
    }

    public Usuario getSession() {
        Usuario usuario = new Usuario();

        usuario.setId(sesion.getInt(DBInfo.UsuariosColumnaID,0));
        usuario.setNombre(sesion.getString(DBInfo.UsuariosColumnaNombre,null));
        usuario.setEmail(sesion.getString(DBInfo.UsuariosColumnaEmail,null));
        usuario.setEmail(sesion.getString(DBInfo.UsuariosColumnaEmail,null));

        return usuario;
    }
    public boolean is_active() {
        IS_ACTIVE = sesion.getBoolean(_IS_ACTIVE, false);
        return IS_ACTIVE;
    }
    public void logOut() {
        editor.clear();
        editor.commit();
    }
}
